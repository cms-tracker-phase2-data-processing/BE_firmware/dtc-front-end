-- Author : Kirika Uchida
-- Date   : 11/09/2020

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;

-- data_i in this example from channel buffer.  (Not 100 % sure how it should be used)

--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0); 
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--  
-- I defined the data_i.start = '1' for the first 32 bits of 40 MHz data.
-- data_i.valid = '1' when the data is there.
-- data_i.strobe is ignored.
-- data_i.data(31 downto 0) carry the data to be sent to gbt at user_data field.
-- The first data_i.data with data_i.start = '1' are user_data_o(31 downto 0), the next data are user_data_o(63 downto 32), the third data (17 downto 0) are user_data_o(79 downto 62). 
-- N_EC_SPARE is 0 for this example.

-- the user_data_o is in clk40_i 

entity emp_tx_user_data_framer is
	generic (
	         INDEX          : integer;
	         CHANNEL_INDEX  : integer := 0;
	    	 N_EC_SPARE     : integer range 0 to 16 := 0
	);
	port (
	    clk_i              : in std_logic;  --- for ipbus (31MHz)
	    rst_i              : in std_logic;  --- for ipbus
	    ipb_in             : in ipb_wbus;   --- ipbus data in (from PC)
	    ipb_out            : out ipb_rbus;  --- ipbus data out (to PC)
        clk40_i            : in  std_logic:='0';		
        downlink_rdy_i     : in  std_logic;
        clk_p_i            : in  std_logic;
        data_i             : in  lword; 
        ec_spare_data_i    : in  std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        user_data_o        : out std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0) -- for GBT, data should be at (79 downto 0)
        );   
end emp_tx_user_data_framer;

architecture interface of emp_tx_user_data_framer is    

    signal user_data0_s : std_logic_vector(31 downto 0);
    signal user_data1_s : std_logic_vector(31 downto 0);
    
    signal reset_sync          : std_logic;
    signal clk40_toggle        : std_logic;
    signal clk40_toggle_meta   : std_logic;
    signal clk40_toggle_sync   : std_logic;
    signal clk40_toggle_sync_r : std_logic;				    
begin                 --========####   Architecture Body   ####========-- 

  user_data_o(31 downto 0)        <= user_data1_s  when rising_edge(clk40_i);

  clk40_toggle        <= not clk40_toggle  when rising_edge(clk40_i);
  clk40_toggle_meta   <= clk40_toggle      when rising_edge(clk_p_i);
  clk40_toggle_sync   <= clk40_toggle_meta when rising_edge(clk_p_i);
  clk40_toggle_sync_r <= clk40_toggle_sync when rising_edge(clk_p_i);
 
  p_reset_sync : process(clk_p_i)
  begin
    if (rising_edge(clk_p_i)) then
      if (downlink_rdy_i='0') then
          reset_sync <= '1';      
      elsif(clk40_toggle_sync_r/=clk40_toggle_sync) then
          reset_sync <= '0';  
      end if;
    end if;
  end process p_reset_sync; 
 
    user_data_o(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 32) <= (others => '0');
    user_data_o(31 downto 0) <= user_data1_s;

	process(reset_sync, clk_p_i)
		variable timer    : integer range 0 to 7;		
		variable dlatched : boolean;
	begin

		if reset_sync = '1' then
		
			timer := 0;
      user_data0_s <= (others => '0');
      user_data1_s <= (others => '0');

		elsif rising_edge(clk_p_i) then
            
            if timer = 0 then
                user_data1_s <= (others => '0');
                if dlatched then
                    user_data1_s(31 downto 0) <= user_data0_s;
                    dlatched := false;
                end if;
                timer := 0;
            else
                timer := timer + 1;
            end if;
            
            if data_i.valid = '1' then
            
                if data_i.start = '1' then
                
                    user_data0_s(31 downto 0) <= data_i.data(31 downto 0);
                    dlatched := true;                                
                end if;
            
            else
                user_data0_s <= (others => '0');
            end if;
            
        end if;

	end process;
        
   
   
end interface;
