-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;


-- entirely combinatorial data framer for Outer Tracker - all module types (tbd), based on 2S GBT-SEH scheme
-- data_i is registered (twice) before framer, and user_data_o is registered (twice) after framer too
-- relies only on data_i input from payload being stable for 8 clocks (at 320MHz clk_p) for correct operation of modules
-- xGBT user data frame map would be dependent on hybrid layout / elink mapping only

-- assignment of fast commands to data_i lword is as follows:
--
-- [ Hybrid 0 / Right, Hybrid 1 / Left ]    Fast Command 
--
--          data_i.data [0,  16]        Orbit/Counter Reset
--          data_i.data [4,  20]        Test Pulse Trigger
--          data_i.data [8,  24]        L1A/Trigger
--          data_i.data [12, 28]        Fast Reset/Resync
--


entity emp_tx_user_data_framer is
	generic (
	         INDEX          : integer;
	         CHANNEL_INDEX  : integer := 0;
	    	 N_EC_SPARE     : integer range 0 to 16 := 0
	);
	port (
	    clk_i              : in std_logic;  --- for ipbus (31MHz)
	    rst_i              : in std_logic;  --- for ipbus
	    ipb_in             : in ipb_wbus;   --- ipbus data in (from PC)
	    ipb_out            : out ipb_rbus;  --- ipbus data out (to PC)
        downlink_rdy_i     : in  std_logic;
        clk_p_i            : in  std_logic;
        data_i             : in  lword; 
        ec_spare_data_i    : in  std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        clken_o            : out std_logic;
        user_data_o        : out std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0)
        );   
end emp_tx_user_data_framer;

architecture interface of emp_tx_user_data_framer is    
			    
begin                 --========####   Architecture Body   ####========-- 


    clken_o <= '1';  -- hold high (at least for GBT)

    user_data_o(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 88) <= (others => '0');
    
    user_data_o(87 downto 80) <= "110" & data_i.data(12) & data_i.data(8) & data_i.data(4) & data_i.data(0) & "1";     -- hybrid 0, right
    
    user_data_o(79 downto 72) <= "110" & data_i.data(28) & data_i.data(24) & data_i.data(20) & data_i.data(16) & "1";  -- hybrid 1, left
    
    user_data_o(71 downto  0) <= (others => '0');

   
end interface;
