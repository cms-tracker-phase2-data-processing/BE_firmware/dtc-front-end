-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;
use work.gbt_framer_data_types.all;
use work.gbt_module_constants.all;
use work.gbt_framer_constants.all;


--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0);
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--
-- data_i in this example.  (Not 100 % sure how it should be used)
-- I defined the data_i.start = '1' for the first 32 bits of 40 MHz data.
-- data_i.valid = '1' when the data is there.
-- data_i.strobe = '1'.

-- user_data_i(111 downto 0) carry the data from gbt at user_data field.

-- user_data_i are latched at clken_i = '1'
-- latched data are assigned to data_o(31 downto 0) in three clocks.
-- N_EC_SPARE is 0 for this example, so ec_spare_data_i is not filling user_data_o.

--      the user_data_i is in clk_p_i domain with clken_i signal (single clock pulse in every 8 clock).

entity emp_rx_user_data_framer is
    generic (
        INDEX             : integer;
        CHANNEL_INDEX     : integer range 0 to 3;
        N_EC_SPARE        : integer range 0 to 16 := 0
    );
    port (
        --- Input Ports ---
        clk40_i                      : in  std_logic:='0';
        clk_p_i                      : in  std_logic;
        uplink_rdy_i                 : in  std_logic;
        user_data_i                  : in  std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0); -- For GBT, WIDE_BUS data are at (111 downto 0), GBT_FRAME data are at (79 downto 0)
        --- Ouput Ports ---
        data_o                       : out  lword;
        ec_spare_data_o              : out std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        --- IPBus Ports ---
        clk_i                        : in std_logic;  --- for ipbus (31MHz)
        rst_i                        : in std_logic;  --- for ipbus
        ipb_in                       : in ipb_wbus;   --- ipbus data in (from PC)
        ipb_out                      : out ipb_rbus  --- ipbus data out (to PC)
    );
end emp_rx_user_data_framer;

architecture interface of emp_rx_user_data_framer is

    signal data_s       : lword                                         := LWORD_NULL;
    signal uplink_data  : std_logic_vector(cGBTFrameWidth - 1 downto 0) := (others => '0');
    signal data_enabled : std_logic                                     := '0';
    type tL1AData is array(0 to 1) of std_logic_vector(7 downto 0);
    signal l1a_data     : tL1AData                                      := (others => (others => '0'));

    signal stub_links          : tMultiCICLinkArray := NullMultiCICLinkArray;
    signal previous_stub_links : tMultiCICLinkArray := NullMultiCICLinkArray;
    signal new_data            : std_logic          := '0';

    signal previous_data : std_logic_vector(cGBTFrameWidth - 1 downto 0) := (others => '0');

    signal stream_valid : std_logic := '0';
    signal time_index   : integer   := 0;

    signal clken_i           : std_logic := '0';
    signal clk40_toggle_prev : std_logic := '0';
    signal clk40_toggle      : std_logic := '0';
    signal clk40_toggle_meta : std_logic := '0';

begin


    clk40_toggle        <= not clk40_toggle  when rising_edge(clk40_i);
    clk40_toggle_meta   <= clk40_toggle      when rising_edge(clk_p_i);
    pClkEnable : process(clk_p_i)
    begin
        if rising_edge(clk_p_i) then
            clk40_toggle_prev <= clk40_toggle_meta;
            clken_i           <= clk40_toggle_meta xor clk40_toggle_prev;
        end if;
    end process;

    pMain : process(clk_p_i)
    begin
        if rising_edge(clk_p_i) then
            if clken_i = '1' then
                stream_valid <='1';
                for i in 0 to cNumberOfCICs - 1 loop
                    for j in 0 to cNumberOfELinks - 1 loop
                        if link_offsets(i)(j) = 0 then
                            stub_links(i)(j)(7 downto 0) <= user_data_i(link_indices(i)(j) + 7 downto link_indices(i)(j));
                            previous_stub_links(i)(j) <= stub_links(i)(j);
                        elsif link_offsets(i)(j) > 0 then
                            stub_links(i)(j)(7 - link_offsets(i)(j) downto 0) <= user_data_i(link_indices(i)(j) + 7 downto link_indices(i)(j) + link_offsets(i)(j));
                            previous_stub_links(i)(j)(7 downto 7 - (link_offsets(i)(j) - 1)) <= user_data_i(link_indices(i)(j) + (link_offsets(i)(j) - 1) downto link_indices(i)(j));
                            previous_stub_links(i)(j)(7 - link_offsets(i)(j) downto 0) <= stub_links(i)(j)(7 - link_offsets(i)(j) downto 0);
                        elsif link_offsets(i)(j) < 0 then
                            stub_links(i)(j)(7 downto -link_offsets(i)(j)) <= user_data_i(link_indices(i)(j) + 7 + link_offsets(i)(j) downto link_indices(i)(j));
                            stub_links(i)(j)(-link_offsets(i)(j) - 1 downto 0) <= previous_data(link_indices(i)(j) + 7 downto link_indices(i)(j) + 7 - (-link_offsets(i)(j) - 1));
                            previous_stub_links(i)(j) <= stub_links(i)(j);
                        end if;
                    end loop;
                    l1a_data(i) <= user_data_i(l1a_indices(i) + 7 downto l1a_indices(i));
                end loop;
                time_index <= cDataPeriod - 1;
                previous_data <= user_data_i(cGBTFrameWidth - 1 downto 0);
            end if;
            -- Transpose link array ---
            if stream_valid = '1' then
                if time_index = 0 then
                    if clken_i = '0' then
                        stream_valid <= '0';
                    end if;
                else
                    time_index <= time_index - 1;
                end if;
                data_s.strobe <= '1';
                data_s.valid <= '1';
                data_s.start <= '1';
                for i in 0 to cNumberOfCICs - 1 loop
                    data_s.data(32*i + 31) <= '1';
                    for j in 0 to cNumberOfELinks - 1 loop
                        data_s.data(32*i + cNumberOfELinks - 1 - j) <= previous_stub_links(i)(j)(time_index);
                        --- Stream L1A link ---
                        data_s.data(32*i + cNumberOfELinks) <= l1a_data(i)(time_index);
                    end loop;
                end loop;
            else
                data_s <= LWORD_NULL;
            end if;
        end if;
    end process pMain;

    data_o <= data_s;
    ec_spare_data_o(N_EC_SPARE * 2 - 1 downto 0) <= user_data_i(N_EC_SPARE * 2 - 1 downto 0);

end interface;
