-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;
use work.gbt_framer_data_types.all;
use work.gbt_module_constants.all;
use work.gbt_framer_constants.all;


entity emp_rx_user_data_framer is
    generic (
        INDEX             : integer;
        CHANNEL_INDEX     : integer range 0 to 3;
        N_EC_SPARE        : integer range 0 to 16 := 0
    );
    port (
        --- Input Ports ---
        clk40_i                      : in  std_logic;
        clk_p_i                      : in  std_logic;
        uplink_rdy_i                 : in  std_logic;
        user_data_i                  : in  std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        --- Ouput Ports ---
        data_o                       : out  lword;
        ec_spare_data_o              : out std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        --- IPBus Ports ---
        clk_i                        : in std_logic;  --- for ipbus (31MHz)
        rst_i                        : in std_logic;  --- for ipbus
        ipb_in                       : in ipb_wbus;   --- ipbus data in (from PC)
        ipb_out                      : out ipb_rbus   --- ipbus data out (to PC)

    );
end emp_rx_user_data_framer;

architecture interface of emp_rx_user_data_framer is

    signal data_s       : lword              := LWORD_NULL;
    
    type tL1AData is array(0 to 1) of std_logic_vector(7 downto 0);
    signal l1a_data     : tL1AData           := (others => (others => '0'));

    signal stub_links   : tMultiCICLinkArray := NullMultiCICLinkArray;


begin


    pMain : process(clk_p_i)
      variable time_index : unsigned(2 downto 0) := (others=>'0');
    begin
        if rising_edge(clk_p_i) then

          time_index := time_index - 1;

          if (time_index = 0) then

              for i in 0 to cNumberOfCICs - 1 loop
                  for j in 0 to cNumberOfELinks - 1 loop
                      stub_links(i)(j)(7 downto 0) <= user_data_i(link_indices(i)(j) + 7 downto link_indices(i)(j));
                  end loop;
                  l1a_data(i) <= user_data_i(l1a_indices(i) + 7 downto l1a_indices(i));
              end loop;

          end if;

          if (uplink_rdy_i = '1') then

              -- Iterate over 2D arrays, sending a slice per clock cycle
              for i in 0 to cNumberOfCICs - 1 loop
                  data_s.data(32*i + 31) <= '1';
                  for j in 0 to cNumberOfELinks - 1 loop
                      data_s.data(32*i + cNumberOfELinks - 1 - j) <= stub_links(i)(j)(to_integer(time_index));
                      --- Stream L1A link ---
                      data_s.data(32*i + cNumberOfELinks) <= l1a_data(i)(to_integer(time_index));
                  end loop;
              end loop;

              data_s.valid  <= '1';
              data_s.strobe <= '1';
              data_s.start  <= '1';

          else

              data_s <= LWORD_NULL;

          end if;

        end if;
    end process pMain;

    data_o <= data_s;
    ec_spare_data_o(N_EC_SPARE * 2 - 1 downto 0) <= user_data_i(N_EC_SPARE * 2 - 1 downto 0);

end interface;

