----
-- Author: David Monk

-- Description file for HeaderExtractor entity.
-- This entity takes in a stream of words whose length is equal to the number of
-- e-links on the front-end module, along with a flag for the start of the boxcar.
-- The entity converts this stream into stubs, which are sent out as soon as
-- possible, meaning that they are not buffered until the end of the boxcar.
----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.gbt_module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;

entity HeaderExtractor is
    port (
        --- Input Ports ---
        clk          : in std_logic;
        header_frame : in integer range 0 to cHeaderFrames + 1;
        frame        : in std_logic_vector(cNumberOfELinks + cStubWidth - 1 downto 0) := (others => '0');
        --- Output Ports ---
        header_out   : out tCICHeader;
        header_mode  : out std_logic
    );
end HeaderExtractor;


architecture Behavorial of HeaderExtractor is
    signal cic_header : tCICHeader := ('0', (others => '0'), (others => '0'), (others => '0'));
    signal header_mode_signal : std_logic := '0';

begin

    header_out <= cic_header;
    header_mode <= header_mode_signal;

    pMain: process(clk)
    begin
        if rising_edge(clk) then
            -- Header Extraction
            if header_frame = 0 then
                cic_header.CIC_conf <= frame(5);
                cic_header.status(8 downto 4) <= frame(4 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 1 then
                cic_header.status(3 downto 0) <= frame(5 downto 2);
                cic_header.bcid(11 downto 10) <= frame(1 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 2 then
                cic_header.bcid(9 downto 4) <= frame(5 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 3 then
                cic_header.bcid(3 downto 0) <= frame(5 downto 2);
                cic_header.stub_count(5 downto 4) <= frame(1 downto 0);
                header_mode_signal <= '0';
            elsif header_frame = 4 then
                cic_header.stub_count(3 downto 0) <= frame(5 downto 2);
                header_mode_signal <= '0';
            end if;
        end if;
    end process;

end Behavorial;
