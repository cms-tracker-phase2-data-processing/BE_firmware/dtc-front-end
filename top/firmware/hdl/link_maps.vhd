library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package dtc_link_maps is
    constant cNumberOfFEModules   : integer := 4;
    constant cNumberOfOutputLinks : integer := 4;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of integer;
    constant cDTCInputLinkMap     : tDTCInputLinkMap := (8, 9, 10, 11);

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap    : tDTCOutputLinkMap := (12, 13, 14, 15);
end package dtc_link_maps;
