library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


package gbt_module_constants is
    constant cGBTFrameWidth           : integer := 112;
    constant cNumberOfELinks          : integer := 5;
    constant cNumberOfCICs            : integer := 2;

    constant cStubWidth               : integer := 18;
    constant cHeaderWidth             : integer := 28;
    constant cMaxStubs                : integer := 16;
    constant cBoxCarFrames            : integer := 64;
    constant cHeaderFrames            : integer := 6;
    constant cFirstStubOffset         : integer := 3;

    constant cDataPeriod              : integer := 8;

    constant cModuleBendWidth         : integer := 4;
    constant cModuleRowWidth          : integer := 11;
    constant cModuleStubBxWidth       : integer := 3;
    constant cHeaderMultiplicityWidth : integer := 6;
    constant cHeaderBxWidth           : integer := 12;

    constant cAlignmentDepth          : integer := 8; -- Can be reduced if no realignment is necessary, reducing both latency and resource usage for the FrameAligner
end package gbt_module_constants;
