----
-- Author: David Monk
-- Description file for HeaderFingerprintChecker entity



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gbt_module_constants.all;
use work.emp_data_types.all;

entity HeaderFingerprintChecker is
    port (
        --- Input Ports ---
        clk     : in std_logic;
        data_in : in lword     := LWORD_NULL;
        --- Output Ports ---
        header_match : out std_logic := '0'
    );
end HeaderFingerprintChecker;


architecture Behavorial of HeaderFingerprintChecker is

    type tRollingWindow is array(70 downto 0) of std_logic_vector(cNumberOfELinks - 1 downto 0);
    signal rolling_window        : tRollingWindow       := (others => (others => '0'));
    signal header_match_signal   : std_logic            := '0';

    constant cBCIDTapPoint      : integer := 0;
    constant cPaddingTapPoint   : integer := 4;

begin
    header_match <= header_match_signal;

    pMain : process(clk)
        variable previous_bcid : std_logic_vector(9 downto 0);
        variable bcid          : std_logic_vector(9 downto 0);
    begin
        if rising_edge(clk) and data_in.strobe = '1' then
            -- Update rolling window with new data
            rolling_window <= rolling_window(rolling_window'high - 1 downto 0) & data_in.data(cNumberOfELinks - 1 downto 0);
            previous_bcid := rolling_window(cBCIDTapPoint + cBoxCarFrames + 1) & rolling_window(cBCIDTapPoint + cBoxCarFrames);
            bcid := rolling_window(cBCIDTapPoint + 1) & rolling_window(cBCIDTapPoint);
            header_match_signal <= '0';

            -- Conditions for header
            if rolling_window(cBoxCarFrames + cPaddingTapPoint)(3 downto 0) = b"0000" and rolling_window(cPaddingTapPoint)(3 downto 0) = b"0000" then
                if unsigned(previous_bcid) + 2 = unsigned(bcid) then
                    header_match_signal <= '1';
                end if;
            end if;
        end if;
    end process;
end architecture;
